# Method 2: monte carlo approximation
def montecarlobs(S0, K, T, r, q, vol, Nsteps):
    payoff = 0.0
    sfin = []
    for i in range(Nsteps):
        rnd = random.gauss(0, sqrt(T))
        sfinal = S0 * exp((r - q - 0.5 * vol ** 2) * T + vol * rnd)
        sfin.append(sfinal)
        payoff += max(sfinal - K, 0)
        payoff = payoff * exp(-r * T) / float(Nsteps)
        return payoff
AnalyticalValues = []
MontecarloValues = []
for k in strikes:
    AnalyticalValues.append(blackscholes(S0, k, T, r, q, vol))
    MontecarloValues.append(montecarlobs(S0, k, T, r, q, vol, Nsteps))
    # Method 3: Reading from data
    datapath = os.path.abspath("../Data/BlackScholes.csv")
    df_random = pd.read_csv(datapath, sep=";")
    print(df_random.head())
# Show and save the plot
fig = plt.figure(1)
plt.plot(strikes, AnalyticalValues, 'b-',
         strikes, MontecarloValues, 'ro',
         df_random['STRIKES'], df_random['OPTIONVAL'], 'g+')
plt.xlabel('Strike')
plt.ylabel('Option Value')
plt.title('European Call Option')
plt.grid(True)
plt.show()
fig.savefig(os.path.abspath("../Results/BlackScholes.png"))


